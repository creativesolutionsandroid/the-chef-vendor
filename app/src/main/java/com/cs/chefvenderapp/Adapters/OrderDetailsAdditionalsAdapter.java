package com.cs.chefvenderapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.chefvenderapp.Constants;
import com.cs.chefvenderapp.Models.OrderDetailsList;
import com.cs.chefvenderapp.R;

import java.util.ArrayList;

public class OrderDetailsAdditionalsAdapter extends RecyclerView.Adapter<OrderDetailsAdditionalsAdapter.MyViewHolder> {

    Context context;
    ArrayList<OrderDetailsList.AdditionalItems> orderLists = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid, lanuguage;

    public OrderDetailsAdditionalsAdapter(Context context, ArrayList<OrderDetailsList.AdditionalItems> ordersCounts, String lanuguage) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.lanuguage = lanuguage;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (lanuguage.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_details_additionals, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_details_additionals_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        if (lanuguage.equalsIgnoreCase("En")) {
            holder.add_name_qty.setText("" + orderLists.get(position).getAddItem_En() + " X " + orderLists.get(position).getAddQuantity());
            holder.add_price.setText("" + Constants.decimalFormat.format(orderLists.get(position).getAddItemPrice()) + " SAR");
        } else {
            holder.add_name_qty.setText("" + orderLists.get(position).getAddQuantity() + " X " + orderLists.get(position).getAddItem_Ar());
            holder.add_price.setText("" + Constants.decimalFormat.format(orderLists.get(position).getAddItemPrice()) + " SAR");
        }

    }

    @Override
    public int getItemCount() {

        return orderLists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView add_name_qty, add_price;

        public MyViewHolder(final View convertView) {
            super(convertView);

            add_name_qty = (TextView) convertView.findViewById(R.id.add_name_qty);
            add_price = (TextView) convertView.findViewById(R.id.add_price);

        }

    }

}
