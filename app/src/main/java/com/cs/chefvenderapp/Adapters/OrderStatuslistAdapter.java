package com.cs.chefvenderapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.chefvenderapp.Activities.OrderTypeActivity;
import com.cs.chefvenderapp.Models.OrderStatusList;
import com.cs.chefvenderapp.R;

import java.util.ArrayList;

public class OrderStatuslistAdapter extends RecyclerView.Adapter<OrderStatuslistAdapter.MyViewHolder> {

    Context context;
    ArrayList<OrderStatusList.OrdersCounts> orderLists = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid;

    public OrderStatuslistAdapter(Context context, ArrayList<OrderStatusList.OrdersCounts> ordersCounts) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_status_list, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.order_status.setText("" + orderLists.get(position).getNewOrders());
        holder.order_status_count.setText("");

    }

    @Override
    public int getItemCount() {

        return orderLists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView order_status, order_status_count;

        public MyViewHolder(final View convertView) {
            super(convertView);

            order_status = (TextView) convertView.findViewById(R.id.order_status);
            order_status_count = (TextView) convertView.findViewById(R.id.order_status_count);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, OrderTypeActivity.class);
                    context.startActivity(a);

                }
            });
        }

    }

}
