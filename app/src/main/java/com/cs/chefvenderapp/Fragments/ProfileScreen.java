package com.cs.chefvenderapp.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.cs.chefvenderapp.Activities.ChangePasswordActivity;
import com.cs.chefvenderapp.Activities.SignInActivity;
import com.cs.chefvenderapp.R;

public class ProfileScreen extends Fragment implements View.OnClickListener{

    TextView tvChangePassword, tvLogout, app_version;
    EditText tvName, tvMobile, tvEmail;
    String strName, strMobile, strEmail;
//    ImageView back_btn;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    Context context;
    Toolbar toolbar;
    AlertDialog loaderDialog = null;

    private static String TAG = "TAG";
    private static int EDIT_REQUEST = 1;
    View rootView;
    SharedPreferences languagePrefs;
    String language;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.my_account, container, false);
        }else if (language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.my_account_arabic, container, false);
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");
        userPrefsEditor = userPrefs.edit();

        tvName = (EditText) rootView.findViewById(R.id.edit_profile_input_name);
        tvEmail = (EditText) rootView.findViewById(R.id.edit_profile_input_email);
        tvMobile = (EditText) rootView.findViewById(R.id.edit_profile_input_mobile);

//        back_btn = (ImageView) rootView.findViewById(R.id.back_btn);

        tvLogout = (TextView) rootView.findViewById(R.id.logout);
        tvChangePassword = (TextView) rootView.findViewById(R.id.change_password);

        app_version = (TextView) rootView.findViewById(R.id.app_version);

        setTypeface();

//        back_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                getActivity().finish();
//
//            }
//        });

        tvName.setText(userPrefs.getString("name","-"));
        tvEmail.setText(userPrefs.getString("email","-"));
        tvMobile.setText("+"+userPrefs.getString("mobile","-")+" - ");

        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        app_version.setText("App Version " + version);

        tvChangePassword.setOnClickListener(this);
        tvLogout.setOnClickListener(this);

        return rootView;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface(){
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.change_password:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                break;

            case R.id.logout:

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.alert_dialog;
                } else {
                    layout = R.layout.alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.app_name));
                    desc.setText("Are You Sure to Logout ?");
                } else {
                    title.setText(getResources().getString(R.string.app_name_ar));
                    desc.setText("هل انت متأكد من تسجيل الخروج؟");
                }

                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userPrefsEditor.clear();
                        userPrefsEditor.commit();

                        Intent a = new Intent(getActivity(), SignInActivity.class);
                        startActivity(a);
                        getActivity().finish();

                        finalCustomDialog.dismiss();
                    }
                });

                final AlertDialog finalCustomDialog1 = customDialog;
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finalCustomDialog1.dismiss();

                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

                break;
        }
    }
}
