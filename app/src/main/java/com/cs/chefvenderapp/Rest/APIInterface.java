package com.cs.chefvenderapp.Rest;

import com.cs.chefvenderapp.Models.ChangeLanguageList;
import com.cs.chefvenderapp.Models.ChangePasswordlist;
import com.cs.chefvenderapp.Models.ForgetPassword;
import com.cs.chefvenderapp.Models.OrderDetailsList;
import com.cs.chefvenderapp.Models.OrderStatusList;
import com.cs.chefvenderapp.Models.OrderTypelist;
import com.cs.chefvenderapp.Models.ResetPasswordlist;
import com.cs.chefvenderapp.Models.Sign_In;
import com.cs.chefvenderapp.Models.UpdateOrderList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

//import com.cs.chefvenderapp.Models.OrderStatus;

public interface APIInterface {

    @POST("PartnerAuthenticationAPI/NewPartnerAuthentication")
    Call<Sign_In> getSignIn(@Body RequestBody body);

    @POST("PartnerAuthenticationAPI/NewForgotPassword")
    Call<ForgetPassword> forgotPassword(@Body RequestBody body);

    @POST("PartnerAuthenticationAPI/NewGetPartnerOrders")
    Call<OrderTypelist> orderstatus(@Body RequestBody body);

    @POST("PartnerAuthenticationAPI/ChangePassword")
    Call<ChangePasswordlist> getchangepass(@Body RequestBody body);

    @POST("PartnerAuthenticationAPI/SetNewPassword")
    Call<ResetPasswordlist> getresetpass(@Body RequestBody body);

    @POST("OrdersAdminUserAPI/UpdateOrderStatus")
    Call<UpdateOrderList> getupdateorder(@Body RequestBody body);

    @POST("PartnerAuthenticationAPI/NewOrdersCounts")
    Call<OrderStatusList> getorderstatus(@Body RequestBody body);

    @POST("OrderInformation/GetOrderTracking")
    Call<OrderDetailsList> getorderdetails(@Body RequestBody body);

    @POST("UserAPI/languageChange")
    Call<ChangeLanguageList> getchangelang(@Body RequestBody body);

 }
