package com.cs.chefvenderapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderTypelist {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("OrderItems")
        private ArrayList<OrderItems> OrderItems;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("Longitude")
        private double Longitude;
        @Expose
        @SerializedName("Latitude")
        private double Latitude;
        @Expose
        @SerializedName("UserAddress")
        private String UserAddress;
        @Expose
        @SerializedName("PaymentMode_En")
        private String PaymentMode_En;
        @Expose
        @SerializedName("OrderMode_En")
        private String OrderMode_En;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("VatPercentage")
        private double VatPercentage;
        @Expose
        @SerializedName("VatCharges")
        private double VatCharges;
        @Expose
        @SerializedName("DeliveryCharges")
        private double DeliveryCharges;
        @Expose
        @SerializedName("DiscountAmount")
        private double DiscountAmount;
        @Expose
        @SerializedName("SubTotal")
        private double SubTotal;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("ExpectedTime")
        private String ExpectedTime;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("Mobile")
        private String Mobile;
        @Expose
        @SerializedName("CustomerName")
        private String CustomerName;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("StoreAddress")
        private String StoreAddress;
        @Expose
        @SerializedName("StoreMobile")
        private String StoreMobile;
        @Expose
        @SerializedName("StoreLongitude")
        private double StoreLongitude;
        @Expose
        @SerializedName("StoreLatitude")
        private double StoreLatitude;
        @Expose
        @SerializedName("BranchName")
        private String BranchName;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("BranchCode")
        private String BranchCode;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;

        public ArrayList<OrderItems> getOrderItems() {
            return OrderItems;
        }

        public void setOrderItems(ArrayList<OrderItems> OrderItems) {
            this.OrderItems = OrderItems;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public double getLongitude() {
            return Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLatitude() {
            return Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public String getUserAddress() {
            return UserAddress;
        }

        public void setUserAddress(String UserAddress) {
            this.UserAddress = UserAddress;
        }

        public String getPaymentMode_En() {
            return PaymentMode_En;
        }

        public void setPaymentMode_En(String PaymentMode_En) {
            this.PaymentMode_En = PaymentMode_En;
        }

        public String getOrderMode_En() {
            return OrderMode_En;
        }

        public void setOrderMode_En(String OrderMode_En) {
            this.OrderMode_En = OrderMode_En;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public double getVatPercentage() {
            return VatPercentage;
        }

        public void setVatPercentage(double VatPercentage) {
            this.VatPercentage = VatPercentage;
        }

        public double getVatCharges() {
            return VatCharges;
        }

        public void setVatCharges(double VatCharges) {
            this.VatCharges = VatCharges;
        }

        public double getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(double DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public double getDiscountAmount() {
            return DiscountAmount;
        }

        public void setDiscountAmount(double DiscountAmount) {
            this.DiscountAmount = DiscountAmount;
        }

        public double getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(double SubTotal) {
            this.SubTotal = SubTotal;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getExpectedTime() {
            return ExpectedTime;
        }

        public void setExpectedTime(String ExpectedTime) {
            this.ExpectedTime = ExpectedTime;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getCustomerName() {
            return CustomerName;
        }

        public void setCustomerName(String CustomerName) {
            this.CustomerName = CustomerName;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public String getStoreAddress() {
            return StoreAddress;
        }

        public void setStoreAddress(String StoreAddress) {
            this.StoreAddress = StoreAddress;
        }

        public String getStoreMobile() {
            return StoreMobile;
        }

        public void setStoreMobile(String StoreMobile) {
            this.StoreMobile = StoreMobile;
        }

        public double getStoreLongitude() {
            return StoreLongitude;
        }

        public void setStoreLongitude(double StoreLongitude) {
            this.StoreLongitude = StoreLongitude;
        }

        public double getStoreLatitude() {
            return StoreLatitude;
        }

        public void setStoreLatitude(double StoreLatitude) {
            this.StoreLatitude = StoreLatitude;
        }

        public String getBranchName() {
            return BranchName;
        }

        public void setBranchName(String BranchName) {
            this.BranchName = BranchName;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getBranchCode() {
            return BranchCode;
        }

        public void setBranchCode(String BranchCode) {
            this.BranchCode = BranchCode;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }
    }

    public static class OrderItems {
        @Expose
        @SerializedName("AdditionalItems")
        private ArrayList<AdditionalItems> AdditionalItems;
        @Expose
        @SerializedName("itemprice")
        private double itemprice;
        @Expose
        @SerializedName("ItemComment")
        private String ItemComment;
        @Expose
        @SerializedName("ItemQuantity")
        private int ItemQuantity;
        @Expose
        @SerializedName("ItemImage")
        private String ItemImage;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;

        public ArrayList<AdditionalItems> getAdditionalItems() {
            return AdditionalItems;
        }

        public void setAdditionalItems(ArrayList<AdditionalItems> AdditionalItems) {
            this.AdditionalItems = AdditionalItems;
        }

        public double getItemprice() {
            return itemprice;
        }

        public void setItemprice(double itemprice) {
            this.itemprice = itemprice;
        }

        public String getItemComment() {
            return ItemComment;
        }

        public void setItemComment(String ItemComment) {
            this.ItemComment = ItemComment;
        }

        public int getItemQuantity() {
            return ItemQuantity;
        }

        public void setItemQuantity(int ItemQuantity) {
            this.ItemQuantity = ItemQuantity;
        }

        public String getItemImage() {
            return ItemImage;
        }

        public void setItemImage(String ItemImage) {
            this.ItemImage = ItemImage;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class AdditionalItems {
        @Expose
        @SerializedName("AddQuantity")
        private int AddQuantity;
        @Expose
        @SerializedName("AddItemPrice")
        private double AddItemPrice;
        @Expose
        @SerializedName("AddItem_Ar")
        private String AddItem_Ar;
        @Expose
        @SerializedName("AddItem_En")
        private String AddItem_En;
        @Expose
        @SerializedName("AddItemId")
        private int AddItemId;

        public int getAddQuantity() {
            return AddQuantity;
        }

        public void setAddQuantity(int AddQuantity) {
            this.AddQuantity = AddQuantity;
        }

        public double getAddItemPrice() {
            return AddItemPrice;
        }

        public void setAddItemPrice(double AddItemPrice) {
            this.AddItemPrice = AddItemPrice;
        }

        public String getAddItem_Ar() {
            return AddItem_Ar;
        }

        public void setAddItem_Ar(String AddItem_Ar) {
            this.AddItem_Ar = AddItem_Ar;
        }

        public String getAddItem_En() {
            return AddItem_En;
        }

        public void setAddItem_En(String AddItem_En) {
            this.AddItem_En = AddItem_En;
        }

        public int getAddItemId() {
            return AddItemId;
        }

        public void setAddItemId(int AddItemId) {
            this.AddItemId = AddItemId;
        }
    }
}
