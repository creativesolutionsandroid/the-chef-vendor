package com.cs.chefvenderapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sign_In {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("UserBrandId")
        private int UserBrandId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("Message")
        private String Message;
        @Expose
        @SerializedName("IsDeleted")
        private boolean IsDeleted;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("OldPassword")
        private String OldPassword;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("NewPassword")
        private String NewPassword;
        @Expose
        @SerializedName("Mobile")
        private String Mobile;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("RoleName")
        private String RoleName;
        @Expose
        @SerializedName("RoleId")
        private int RoleId;
        @Expose
        @SerializedName("Password")
        private String Password;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("AdminId")
        private int AdminId;

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public int getUserBrandId() {
            return UserBrandId;
        }

        public void setUserBrandId(int UserBrandId) {
            this.UserBrandId = UserBrandId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public boolean getIsDeleted() {
            return IsDeleted;
        }

        public void setIsDeleted(boolean IsDeleted) {
            this.IsDeleted = IsDeleted;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getOldPassword() {
            return OldPassword;
        }

        public void setOldPassword(String OldPassword) {
            this.OldPassword = OldPassword;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getNewPassword() {
            return NewPassword;
        }

        public void setNewPassword(String NewPassword) {
            this.NewPassword = NewPassword;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getRoleName() {
            return RoleName;
        }

        public void setRoleName(String RoleName) {
            this.RoleName = RoleName;
        }

        public int getRoleId() {
            return RoleId;
        }

        public void setRoleId(int RoleId) {
            this.RoleId = RoleId;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getAdminId() {
            return AdminId;
        }

        public void setAdminId(int AdminId) {
            this.AdminId = AdminId;
        }
    }
}
